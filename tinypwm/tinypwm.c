/* PWM-ATtiny84
 * Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <math.h>

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>
#include <inttypes.h>
#include <util/atomic.h>

static inline void setup_pwm()
{
  // Pins PA6 (OC1A) get PWM.
  // Fast PWM 10-bit: WGM=0111
  TCCR1A = 0                 //
           | 0b11 << COM1A0  // inverting PWM mode on OC1A
           | 0b00 << COM1B0  // OCR0B disabled
           | 0b11 << WGM10   // fast PWM mode
      ;
  TCCR1B = 0                //
           | 0b01 << WGM12  // fast PWM mode
           | 0b010 << CS00  // clk/8
      ;
  TCCR1C = 0;
  TIMSK1 = 0  //
              //  | 1 << TOIE1  // Enable interrupt
      ;
  OCR1A = 0;
}

static inline void setup_adc()
{
  ADMUX = 0                   //
          | 0b00 << REFS0     // VCC as voltage ref
          | 0b000000 << MUX0  // Read from ADC0 == PA0
      ;
  ADCSRA = 0                 //
           | 1 << ADEN       // Enable ADC
           | 1 << ADIE       // Enable interrupt
           | 0b011 << ADPS0  // scale ADC clock
      ;
  ADCSRB = 0             //
           | 0 << ADLAR  // Right-align
      ;
  DIDR0 = 0             //
          | 1 << ADC0D  // disable digital input on ADC0 = PA0
      ;
}

static inline void setup()
{
  PRR = 0              //
        | 1 << PRTIM0  // Disable Timer 0
        | 1 << PRUSI   // Disable USI
      ;
  // Turn off WDT
  WDTCSR |= (1 << WDCE) | (1 << WDE);
  WDTCSR = 0x00;

  // Configure port A.
  DDRA = 0            //
         | 1 << DDA6  // for PWM output
      ;
  PORTA = 0                //
          | (1 << PORTA1)  //
          | (1 << PORTA2)  //
          | (1 << PORTA3)  //
          | (1 << PORTA4)  //
          | (1 << PORTA5)  //
          | (1 << PORTA7)  //
      ;

  // Port B is not used. Set as input + pullup to save power.
  DDRB = 0;
  PORTB = 0b1111;

  setup_pwm();
  setup_adc();
}

ISR(ADC_vect)
{
}

static inline void readADC()
{
  set_sleep_mode(SLEEP_MODE_IDLE);
  ADCSRA |= 1 << ADSC | 1 << ADIE;
  sleep_mode();
}

static inline void loop()
{
  readADC();

  OCR1A = 1023 - ADC;
}

int main()
{
  sei();
  setup();
  for (;;)
    loop();
  return 0;
}
