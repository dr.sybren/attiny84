/* Copyright (C) 2021 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "pins.h"

#include <avr/interrupt.h>

static inline void setup_adc(void)
{
  ADMUX = 0                //
          | 0b00 << REFS0  // VCC as voltage ref
          | 0 << MUX0      // Read from ADC0 / PA0
      ;
  ADCSRA = 0                 //
           | 1 << ADEN       // Enable ADC
           | 1 << ADATE      // Auto-trigger enable
           | 1 << ADIE       // interrupt required for triggering free-running mode
           | 0b100 << ADPS0  // scale ADC clock so it's 50-200 KHz
      ;
  ADCSRB = 0                 //
           | 0 << ADLAR      // Right-align
           | 0b000 << ADTS0  // Auto-trigger source
      ;
  DIDR0 = 0             //
          | 1 << ADC0D  // disable digital input on ADC0 / PA0
      ;

  // Trigger a conversion to start freerunning mode.
  ADCSRA |= 1 << ADSC;
}

void pins_setup()
{
  DDRA = 1 << DDA1 | 1 << DDA3 | 1 << DDA5;  // OUT on PA1/3/5
  DDRB = 0;                                  // IN on all PB
  PORTA = 0                                  // pull-ups
          | 0 << PORTA2                      // clock in
          | 1 << PORTA4                      // SCL (unused, just to avoid nose)
          | 1 << PORTA6                      // MOSI (unused, just to avoid nose)
          | 1 << PORTA7                      // calibration safe (A7)
      ;
  PORTB = 1 << PORTB2;  // pull-up on calibration input

  setup_adc();
}

ISR(ADC_vect)
{
}
