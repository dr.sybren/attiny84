/* Copyright (C) 2021 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "clockpulse.h"
#include "pins.h"
#include "powerstate.h"

#include <avr/interrupt.h>
#include <avr/io.h>

volatile uint8_t clockpulses_received;
volatile uint8_t clockpulses_sent;
volatile bool clock_out = false;

void clockpulse_setup()
{
  // TODO: Set up clock pulse to send to clock OUT.
  // CTC mode -> WGM = 0b0100
  TCCR1A = 0                 //
           | 0b00 << COM1A0  //
           | 0b00 << COM1B0  //
           | 0b00 << WGM10   //
      ;
  TCCR1B = 0                //
           | 0b01 << WGM12  //
           | 0b100 << CS10  // 1/1024 clockdiv
      ;
  TCCR1C = 0;
  OCR1A = 1023;

  // Call clockpulse_enable() to actually start the clock pulse.
}

void clockpulse_set_period(uint16_t period)
{
  OCR1A = period;
}

ISR(TIM1_COMPA_vect)
{
  pin_write_clockout(clock_out);
  if (clock_out && clockpulses_sent < 254) {
    ++clockpulses_sent;
  }

  clock_out = !clock_out;

  const uint16_t clock_period = ADC + 512;
  clockpulse_set_period(clock_period);
}

void clockpulse_reset()
{
  TCNT1 = 0;  // start counting from 0

  clockpulses_received = 0;
  clockpulses_sent = 0;
}

void clockpulse_received()
{
  if (power_state != PWR_STATE_OFF)
    return;

  if (clockpulses_received < 200)
    ++clockpulses_received;
}

void clockpulse_enable()
{
  TCNT1 = 0;              // start counting from 0
  clock_out = false;      // start with a low value
  TIMSK1 |= 1 << OCIE1A;  // enable interrupt
}
void clockpulse_disable()
{
  TIMSK1 &= ~(1 << OCIE1A);  // disable interrupt
  pin_write_clockout(false);
}
