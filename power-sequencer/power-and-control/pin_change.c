/* Copyright (C) 2021 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "pin_change.h"
#include "clockpulse.h"
#include "pins.h"
#include "powerstate.h"

#include <avr/interrupt.h>
#include <avr/io.h>

void pin_change_setup()
{
  // Pin Change interrupt on PCINT 9 (power state input) and 2 (clock input).
  GIMSK |= 1 << PCIE1 | 1 << PCIE0;
  PCMSK0 = 1 << PCINT2;
  PCMSK1 = 1 << PCINT9;
}

ISR(PCINT0_vect)
{
  const bool clock_pulse = pin_read_clockin();

  if (clock_pulse) {
    clockpulse_received();
  }
}

ISR(PCINT1_vect)
{
  powerstate_update();
  clockpulse_reset();
}
