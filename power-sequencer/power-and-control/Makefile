# ATtiny84 Switching Socket
# Copyright (C) 2021 dr. Sybren A. Stüvel
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
TARGET := $(notdir $(realpath .))
SOURCES := $(wildcard *.c) $(wildcard *.cpp)
OBJECTS := $(patsubst %.c,%.o,$(patsubst %.cpp,%.o,$(SOURCES)))
AVRDUDE := avrdude -v -c usbtiny -B3 -p attiny84 -V

# Add some Windows-specific options for AVRDude, and make sure it can be found.
ifeq ($(OS),Windows_NT)
	ARDUINO_HOME := C:/Program Files (x86)/Arduino
	AVR_HOME := $(ARDUINO_HOME)/hardware/tools/avr
	AVRDUDE += -C "$(AVR_HOME)/etc/avrdude.conf"
    PATH := $(PATH);$(AVR_HOME)/bin
endif


F_CPU := 8000000

CC=avr-gcc
CXX=avr-g++
COMMONFLAGS=-Wall -Werror -Os -mmcu=attiny84 \
	-DF_CPU=$(F_CPU) \
	-Wno-unused-function \
	-I.. \
	-idirafter /usr/include
# -DF_CPU defines the clock speed in the micro-controller.
# -idirafter /usr/include is for including /usr/include/simavr/avr/avr_mcu_section.h
CFLAGS=-std=c99 $(COMMONFLAGS)
CXXFLAGS=-std=c++11 $(COMMONFLAGS)

flash-and-clean: flash clean

$(TARGET).elf: $(OBJECTS)
	$(CC) $(CFLAGS) -o $@ $^

%.hex: %.elf
	avr-objcopy -j .text -j .data -O ihex $< $@

hex: $(TARGET).hex

flash: $(TARGET).hex
	$(AVRDUDE) -U flash:w:$<:i

flash-and-eeprom: $(TARGET).hex
	$(AVRDUDE) -U flash:w:$<:i -U eeprom:w:eeprom.hex:i

fuses-read:
	$(AVRDUDE)

fuses-write:
	$(AVRDUDE) -U lfuse:w:0x62:m -U hfuse:w:0xd7:m -U efuse:w:0xff:m

eeprom-read:
	$(AVRDUDE) -U eeprom:r:eeprom.hex:i

eeprom-write: eeprom.hex
	$(AVRDUDE) -U eeprom:w:eeprom.hex:i

clean:
	@rm -f *.o *.elf $(TARGET).hex

.PHONY: clean fuses flash
