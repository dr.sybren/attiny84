/* Copyright (C) 2021 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "calibration.h"
#include "clockpulse.h"
#include "pin_change.h"
#include "pins.h"
#include "powerstate.h"
#include "timekeeping.h"
#include "zero_volt.h"

#include <stdbool.h>
#include <stdint.h>

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>

int main()
{
  OSCCAL = 116;

  PRR = 0             //
        | 1 << PRUSI  // Disable USI
      ;

  // Turn off WDT
  WDTCSR |= (1 << WDCE) | (1 << WDE);
  WDTCSR = 0x00;

  pins_setup();

  // Wait for pull-up resistors to do their work.
  for (volatile uint16_t counter = 0; counter < 1000; ++counter)
    __asm__ __volatile__("nop");

  pin_change_setup();
  timer_setup();
  powerstate_setup();
  clockpulse_setup();

  sei();

  calibration_setup();
  // End of boot.

  for (;;) {
    switch (power_state) {
      case PWR_STATE_ON:
        clockpulse_disable();

        // Always enable the main power.
        wait_until_safe_to_switch(relay_on_ticks);
        pin_write_relay(true);

        power_state_ack_change_to(PWR_STATE_ON);
        clockpulse_enable();
        break;
      case PWR_STATE_OFF:
        // Don't respond to this until we start receiving clock pulses.
        if (clockpulses_received >= 2 || clockpulses_sent > 20) {
          // This turns off all power. The microcontroller will keep running
          // until capactors have drained.
          wait_until_safe_to_switch(relay_off_ticks);
          pin_write_relay(false);

          power_state_ack_change_to(PWR_STATE_OFF);
          clockpulse_reset();
          clockpulse_disable();
        }
        else {
          pin_write_error_led(true);
          timer_wait_sec(0.1f);
          pin_write_error_led(false);
          timer_wait_sec(0.1f);
        }
        break;
      case PWR_STATE_UNCHANGED:
        break;
    }

    set_sleep_mode(SLEEP_MODE_IDLE);
    sleep_mode();
  }
  return 0;
}
