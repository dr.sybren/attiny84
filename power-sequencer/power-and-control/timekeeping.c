/* Copyright (C) 2021 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "timekeeping.h"

#include <stdint.h>

#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/atomic.h>

volatile uint16_t timer_ticks_since_reset = 0;

void timer_setup()
{
  // WGM = 0b010 for CTC mode.
  TCCR0A = 0                  //
           | (0b10 << WGM00)  // two LSB of wave generation mode
      ;
  TCCR0B = 0                  //
           | (0b0 << WGM02)   // two MSB of wave generation mode
           | (0b001 << CS10)  // prescaler clk/1
      ;
  TIMSK0 = 0                //
           | (1 << OCIE0A)  // Output Compare A interrupt enable
      ;

  OCR0A = 99;  // At room temperature this should result in interrupts at 10 kHz.
  timer_reset();
}

void timer_shutdown()
{
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    TCCR0A = TCCR0B = TIMSK0 = OCR0A = 0;
  }
}

void timer_reset(void)
{
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    TCNT0 = 0;
    timer_ticks_since_reset = 0;
  }
}

void timer_wait_sec(float seconds_to_wait)
{
  const uint16_t ticks_to_wait = seconds_to_wait * 10000;
  timer_wait_ticks(ticks_to_wait);
}

void timer_wait_ticks(uint16_t ticks_to_wait)
{
  timer_reset();
  while (timer_ticks_since_reset < ticks_to_wait)
    ;
}

ISR(TIM0_COMPA_vect)
{
  // Avoid overflows.
  if (timer_ticks_since_reset == 65535) {
    return;
  }

  ++timer_ticks_since_reset;
}
