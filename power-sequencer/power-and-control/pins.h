/* Copyright (C) 2021 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <stdbool.h>

#include <avr/io.h>

/* Pinout:
 * PA0: AIN Clock speed
 * PA1: OUT Relay control
 * PA2: IN  Clock IN
 * PA3: OUT Clock OUT, copies Clock IN only if PA1 == PA2 == PA7
 * PA5: OUT Error LED
 * PA7: IN  Calibration SAFE
 * PB0: IN  Zero detect input, HIGH = safe to switch
 * PB1: IN  Requested switch power state
 * PB2: IN  NEULTRAL connection (calibration IN)
 */

void pins_setup();

// Read from PA1
static inline bool pin_read_relay()
{
  return (PINA & (1 << PINA1)) != 0;
}

// Read from PA2
static inline bool pin_read_clockin()
{
  return (PINA & (1 << PINA2)) != 0;
}

// Read from PB1
static inline bool pin_read_requested_power_state()
{
  return (PINB & (1 << PINB1)) != 0;
}

// Read from PB2
static inline bool pin_read_calibration_input()
{
  return (PINB & (1 << PINB2)) != 0;
}

// Read from PA7
// NOTE: just like other pin_read functions, this returns the state of the pin.
// NOT whether calibration is safe or not, that's a higher-level abstraction.
static inline bool pin_read_calibration_safe()
{
  return (PINA & (1 << PINA7)) != 0;
}

// Read from PB0
static inline bool pin_read_zero_voltage_state()
{
  return (PINB & (1 << PINB0)) != 0;
}

// Write to PA1
static inline void pin_write_relay(bool relay_closed)
{
  if (relay_closed) {
    PORTA |= 1 << PORTA1;
  }
  else {
    PORTA &= ~(1 << PORTA1);
  }
}

// Write to PA3
static inline void pin_write_clockout(const bool high)
{
  if (high) {
    PORTA |= 1 << PORTA3;
  }
  else {
    PORTA &= ~(1 << PORTA3);
  }
}

// Write to PA5
static inline void pin_write_error_led(const bool high)
{
  if (high) {
    PORTA |= 1 << PORTA5;
  }
  else {
    PORTA &= ~(1 << PORTA5);
  }
}
static inline bool pin_read_error_led()
{
  return (PINA & (1 << PINA5)) != 0;
}
