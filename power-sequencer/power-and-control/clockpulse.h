/* Copyright (C) 2021 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

/* Pinout:
 * PB2: PCINT10 Clock IN
 */

#include <stdint.h>

// How many clockpulses were received since it was reset.
// Clockpulses are only counted when power_state = PWR_STATE_OFF.
extern volatile uint8_t clockpulses_received;

// How many clockpulses were sent since it was last reset.
// Will not overflow, so max 255.
extern volatile uint8_t clockpulses_sent;

void clockpulse_setup();
void clockpulse_set_period(uint16_t period);  // 0-1032

void clockpulse_reset();
void clockpulse_received();

void clockpulse_enable();
void clockpulse_disable();
