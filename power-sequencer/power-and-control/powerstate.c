/* Copyright (C) 2021 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "powerstate.h"
#include "pins.h"

#include <util/atomic.h>

void powerstate_setup()
{
  power_state = pin_read_requested_power_state() ? PWR_STATE_ON : PWR_STATE_UNCHANGED;
}

void powerstate_update()
{
  power_state = pin_read_requested_power_state() ? PWR_STATE_ON : PWR_STATE_OFF;
}

void power_state_ack_change_to(PowerState state_to_ack)
{
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    if (power_state != state_to_ack) {
      return;
    }
    power_state = PWR_STATE_UNCHANGED;
  }
}
