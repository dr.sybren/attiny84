/* Copyright (C) 2021 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "zero_volt.h"
#include "pins.h"
#include "timekeeping.h"

static inline void wait_until_zerovolt_pulse(const bool wait_for_state)
{
  for (uint16_t counter = 0; counter < 20000; ++counter) {
    if (pin_read_zero_voltage_state() == wait_for_state)
      return;
  }
  pin_write_error_led(!pin_read_error_led());
}
static inline void wait_until_zerovolt_pulse_high()
{
  wait_until_zerovolt_pulse(true);
}
static inline void wait_until_zerovolt_pulse_low()
{
  wait_until_zerovolt_pulse(false);
}

static uint16_t ticks_between_zerovolt_pulses()
{
  // Measure the time between zero-voltage-state pulses.
  // 50 Hz -> 10ms between pulses -> ~100 ticks per pulse

  wait_until_zerovolt_pulse_low();
  wait_until_zerovolt_pulse_high();
  // Start measuring when a pulse starts.
  timer_reset();
  wait_until_zerovolt_pulse_low();
  wait_until_zerovolt_pulse_high();
  wait_until_zerovolt_pulse_low();
  wait_until_zerovolt_pulse_high();

  const uint16_t ticks_per_pulse = timer_ticks_since_reset / 2;
  return ticks_per_pulse;
}

// Switching takes a bit of a delay, so we have to be a bit early toggling the pin.
void wait_until_safe_to_switch(const uint16_t ticks_to_lead)
{
  const uint16_t ticks_per_pulse = ticks_between_zerovolt_pulses();
  const uint16_t wait_after_pulse = ticks_per_pulse - ticks_to_lead;

  wait_until_zerovolt_pulse_low();
  wait_until_zerovolt_pulse_high();

  // Switch at the start of the pulse. Ideal would be in the middle of the pulse, but the relay
  // bounces and the bounces should also happen as close to zero volts as possible.
  timer_wait_ticks(wait_after_pulse);
}
