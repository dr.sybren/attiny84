/* Copyright (C) 2021 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "clockpulse.h"
#include "pins.h"

#include <avr/interrupt.h>
#include <avr/io.h>


void clockpulse_setup()
{
  // Pin Change interrupt on PCINT 10 (clock input).
  GIMSK |= 1 << PCIE1;
  PCMSK0 = 0;
  PCMSK1 = 1 << PCINT10;

  pin_write_clock_pulse(false);
}

ISR(PCINT1_vect)
{
}
