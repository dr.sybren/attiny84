/* Copyright (C) 2021 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "calibration.h"
#include "pins.h"
#include "timekeeping.h"

#include <avr/eeprom.h>

// EEPROM locations.
uint8_t *const EEPROM_CALIBRATION_PERFORMED = (uint8_t *)(0);
uint16_t *const EEPROM_CALIBRATION_TICKS_RELAY1_ON = (uint16_t *)(1);
uint16_t *const EEPROM_CALIBRATION_TICKS_RELAY2_ON = (uint16_t *)(3);
uint16_t *const EEPROM_CALIBRATION_TICKS_RELAY1_OFF = (uint16_t *)(5);
uint16_t *const EEPROM_CALIBRATION_TICKS_RELAY2_OFF = (uint16_t *)(7);

// Values stored in those locations.
const uint8_t EEPROM_CALIBRATION_PERFORMED_MAGIC_VALUE = 0x47;
uint16_t relay1_on_ticks = 0;
uint16_t relay2_on_ticks = 0;
uint16_t relay1_off_ticks = 0;
uint16_t relay2_off_ticks = 0;

static void calibration_save();
static void calibration_load_defaults();
static bool calibration_allowed();

void calibration_setup()
{
  pin_write_error_led(false);

  if (!calibration_required()) {
    calibration_load();
    return;
  }

  if (calibration_allowed()) {
    calibration_perform();
    return;
  }

  pin_write_error_led(true);
  calibration_load_defaults();
}

bool calibration_required()
{
  return eeprom_read_byte(EEPROM_CALIBRATION_PERFORMED) !=
         EEPROM_CALIBRATION_PERFORMED_MAGIC_VALUE;
}

static inline void calibration_step(const uint8_t relay_num,
                                    uint16_t *r_relay_on_ticks,
                                    uint16_t *r_relay_off_ticks)
{
  // Bring the relay to a known, open state.
  pin_write_relay(relay_num, false);
  while (!pin_read_calibration_input())
    ;

  // Wait a little bit for things to settle down.
  timer_wait_sec(0.1f);

  // Turn on the pin and wait until the calibration input is pulled low.
  timer_reset();
  pin_write_relay(relay_num, true);
  while (pin_read_calibration_input())
    ;
  *r_relay_on_ticks += timer_ticks_since_reset;

  // Wait a little bit for things to settle down.
  timer_wait_sec(0.1f);

  // Turn off the pin and wait until the calibration input is pulled high.
  timer_reset();
  pin_write_relay(relay_num, false);
  while (!pin_read_calibration_input())
    ;
  *r_relay_off_ticks += timer_ticks_since_reset;
}

void calibrate(const uint8_t relay_num, uint16_t *r_relay_on_ticks, uint16_t *r_relay_off_ticks)
{
  // Both relays must be turned off before calibration can start.
  pin_write_relay1(false);
  pin_write_relay2(false);

  // Wiggle the relay around a bit to loosen it up.
  uint16_t dummy;
  for (uint8_t times = 0; times < 5; ++times)
    calibration_step(relay_num, &dummy, &dummy);

  // Perform the actual calibration.
  uint16_t total_ticks_on = 0;
  uint16_t total_ticks_off = 0;
  for (uint8_t times = 0; times < 8; ++times)
    calibration_step(relay_num, &total_ticks_on, &total_ticks_off);

  *r_relay_on_ticks = total_ticks_on >> 3;
  *r_relay_off_ticks = (total_ticks_off >> 3) - 4;
}

void calibration_perform()
{
  calibrate(1, &relay1_on_ticks, &relay1_off_ticks);
  calibrate(2, &relay2_on_ticks, &relay2_off_ticks);
  calibration_save();
}

static void calibration_save()
{
  eeprom_update_word(EEPROM_CALIBRATION_TICKS_RELAY1_ON, relay1_on_ticks);
  eeprom_update_word(EEPROM_CALIBRATION_TICKS_RELAY2_ON, relay2_on_ticks);
  eeprom_update_word(EEPROM_CALIBRATION_TICKS_RELAY1_OFF, relay1_off_ticks);
  eeprom_update_word(EEPROM_CALIBRATION_TICKS_RELAY2_OFF, relay2_off_ticks);
  eeprom_update_byte(EEPROM_CALIBRATION_PERFORMED, EEPROM_CALIBRATION_PERFORMED_MAGIC_VALUE);
}

void calibration_load()
{
  const uint16_t on_ticks_1 = eeprom_read_word(EEPROM_CALIBRATION_TICKS_RELAY1_ON);
  const uint16_t on_ticks_2 = eeprom_read_word(EEPROM_CALIBRATION_TICKS_RELAY2_ON);
  const uint16_t off_ticks_1 = eeprom_read_word(EEPROM_CALIBRATION_TICKS_RELAY1_OFF);
  const uint16_t off_ticks_2 = eeprom_read_word(EEPROM_CALIBRATION_TICKS_RELAY2_OFF);

  // Only use values from EEPROM if they're anywhere sane.
  if (                                             //
      0x10 < on_ticks_1 && on_ticks_1 < 0x50 &&    //
      0x10 < off_ticks_1 && off_ticks_1 < 0x50 &&  //
      0x10 < on_ticks_2 && on_ticks_2 < 0x50 &&    //
      0x10 < off_ticks_2 && off_ticks_2 < 0x50     //
  ) {
    pin_write_error_led(false);
    relay1_on_ticks = on_ticks_1;
    relay1_on_ticks = on_ticks_1;
    relay2_off_ticks = off_ticks_2;
    relay2_off_ticks = off_ticks_2;
  }
  else {
    calibration_load_defaults();
  }
}

static void calibration_load_defaults()
{
  pin_write_error_led(true);

  // known-ok-ish defaults.
  relay1_on_ticks = 0x31;
  relay1_off_ticks = 0x24;
  relay2_on_ticks = 0x31;
  relay2_off_ticks = 0x24;
}

static bool calibration_allowed()
{
  // Calibration is allowed iff the pin is pulled down.
  return pin_read_calibration_not_safe() == false;
}
