/* ATtiny84 Zaklampje
 * Copyright (C) 2021 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "calibration.h"
#include "clockpulse.h"
#include "pins.h"
#include "timekeeping.h"
#include "zero_volt.h"

#include <stdbool.h>
#include <stdint.h>

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>

static void handle_clockpulse();
static void handle_rising_clockpulse();

int main()
{
  OSCCAL = 116;

  pins_setup();
  pin_write_relay1(false);
  pin_write_relay2(false);
  timer_setup();
  clockpulse_setup();

  sei();

  calibration_setup();
  timer_wait_sec(0.5f);

  pin_write_relay1(false);
  pin_write_relay2(false);

  for (;;) {
    handle_clockpulse();
    set_sleep_mode(SLEEP_MODE_IDLE);
    sleep_mode();
  }
  return 0;
}

static void handle_clockpulse()
{
  static bool last_clockpulse = false;
  const bool clockpulse = pin_read_clock_pulse();
  if (clockpulse == last_clockpulse) {
    return;
  }

  last_clockpulse = clockpulse;
  if (clockpulse) {
    handle_rising_clockpulse();
  }
  else {
    pin_write_clock_pulse(false);
  }
}

static void handle_rising_clockpulse()
{
  const bool power_state = pin_read_requested_power_state();

  if (pin_read_relay1() != power_state) {
    // Switch relay 1.
    wait_until_safe_to_switch(power_state ? relay1_on_ticks : relay1_off_ticks);
    pin_write_relay1(power_state);
  }
  else if (pin_read_relay2() != power_state) {
    // Switch relay 2.
    wait_until_safe_to_switch(power_state ? relay2_on_ticks : relay2_off_ticks);
    pin_write_relay2(power_state);
  }
  else {
    // Relays are both good, forward the rising edge to the next device.
    pin_write_clock_pulse(true);
  }
}
