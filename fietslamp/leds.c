/* Copyright (C) 2021 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "leds.h"
#include "pins.h"

#ifdef LEDS_INDIVIDUAL_PWM
#  include "random.h"
#endif

#include <avr/pgmspace.h>

static void leds_setup_timer();

void leds_setup()
{
  pin_write_blank(true);
  leds_write_16bit(0);
  pin_write_blank(false);

  leds_setup_timer();
}

static void leds_setup_timer()
{
  // PWM the LED BLANK output on PB2/OC0A
  // WGM = 0b011 for Fast PWM
  TCCR0A = 0                 //
           | 0b11 << COM0A0  // OC0A inverting
           | 0b00 << COM0B0  // OC0B disconnected
           | 0b11 << WGM00   // mode
      ;
  TCCR0B = 0                //
           | 0b0 << WGM02   // mode
           | 0b010 << CS00  // clkdiv
      ;
  TIMSK0 = 0;
  OCR0A = 16;
}

static const uint8_t light_map_8bit[] PROGMEM = {
    0,  1,  2,  3,   4,   5,   6,   7,   8,   9,   10,  11,  12,  13,  14, 16, 17,
    19, 21, 22, 24,  27,  29,  32,  35,  38,  41,  45,  49,  53,  58,  63, 68, 74,
    81, 87, 95, 103, 112, 122, 132, 144, 156, 169, 184, 199, 216, 235, 255};
void leds_set_pwm(const uint8_t light_level)
{
  OCR0A = pgm_read_byte(&light_map_8bit[light_level]);
}

void leds_write_16bit(uint16_t bit_pattern)
{
  for (uint8_t bit_idx = 0; bit_idx < 16; bit_idx++) {
    const bool bit = (bit_pattern & 1);
    pin_write_serial_data(bit);
    pin_write_serial_clock(true);
    bit_pattern >>= 1;
    pin_write_serial_clock(false);
  }
  pin_write_latch(true);
  pin_write_latch(false);
}

static const uint16_t progress_bits[] PROGMEM = {
    0b0000000000000000,  // 0
    0b0000000000000001,  // 1
    0b0000000000000011,  // 2
    0b0000000000000111,  // 3
    0b0000000000001111,  // 4
    0b0000000010001111,  // 5
    0b0000100010001111,  // 6
    0b1000100010001111,  // 7
    0b1100100010001111,  // 8
    0b1110100010001111,  // 9
    0b1111100010001111,  // 10
    0b1111100110001111,  // 11
    0b1111100110011111,  // 12
};
void leds_progress_bar(const uint8_t progress)  // values 0 (off) to 12 (full)
{
  const uint16_t bit_pattern = pgm_read_word(&progress_bits[progress]);
  leds_write_16bit(bit_pattern);
}

#ifdef LEDS_INDIVIDUAL_PWM
uint8_t leds_level[16];
void leds_levels_update()
{
  uint16_t bit_pattern = 0;

  const uint8_t random_value = random_byte();
  const uint8_t *current_level = leds_level;
  for (int8_t led_index = 0; led_index < 16; ++led_index, ++current_level) {
    const bool led_on = *current_level > random_value;
    bit_pattern |= (led_on << led_index);
  }

  leds_write_16bit(bit_pattern);
}
#endif
