/* Copyright (C) 2021 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "random.h"

#include <stdint.h>

#include <avr/interrupt.h>
#include <avr/io.h>

uint16_t xorshift16_state;

void random_setup()
{
  xorshift16_state = 0xbeef;
}

uint16_t random_xorshift16()
{
  uint16_t x = xorshift16_state;
  x ^= x << 7;
  x ^= x >> 9;
  x ^= x << 8;
  xorshift16_state = x;
  return x;
}
