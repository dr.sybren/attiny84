#include "adc.h"
#include "leds.h"
#include "pins.h"
#include "random.h"
#include "timekeeping.h"

#include <string.h>

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>

static void battery_measure_at_boot();
static void fade_to_white();

int main(void)
{
  // random_setup();
  pins_setup();
  leds_setup();
  adc_setup();
  timer_setup();

  sei();

#if 0
  leds_set_pwm(32);
  // leds_write_16bit(~0);
  for (;;) {
    const uint8_t battery_level = adc_battery_level();
    leds_progress_bar(battery_level);
    // leds_write_16bit(adc_rolling_average());
    timer_wait_ticks(1);
  }
#else
  battery_measure_at_boot();
  leds_write_16bit(~0);

  // These are on a 0 (dim but on) to 48 (brightest) scale:
  const int8_t pwm_min_default = 8;
  const int8_t pwm_max_default = 32;

  int8_t pwm_min = pwm_min_default;
  int8_t pwm_max = pwm_max_default;
  for (;;) {
    const uint8_t battery_level = adc_battery_level();
    if (battery_level < 3) {
      leds_write_16bit(0xa5a5);
      pwm_min = 0;
      pwm_max = pwm_max_default - pwm_min_default;
    }
    else {
      leds_write_16bit(~0);
      pwm_min = pwm_min_default;
      pwm_max = pwm_max_default;
    }

    // Brighten
    for (int8_t pwm_value = pwm_min; pwm_value <= pwm_max; ++pwm_value) {
      leds_set_pwm(pwm_value);
      timer_wait_ticks(12);
    }

    // Hold brightness
    timer_wait_ticks(250);

    // Darken
    for (int8_t pwm_value = pwm_max; pwm_value >= pwm_min; --pwm_value) {
      leds_set_pwm(pwm_value);
      timer_wait_ticks(3);
    }
  }
#endif
  return 0;
}

static void battery_measure_at_boot()
{
  // for (uint8_t counter = 0; counter < 13; ++counter) {
  //   leds_progress_bar(counter);
  //   leds_set_pwm(counter * 4);
  //   timer_wait_ticks(4);
  // }
  // timer_wait_ticks(4);
  // const uint8_t battery_level = adc_battery_level();
  // for (uint8_t counter = 11; counter > battery_level; --counter) {
  //   leds_progress_bar(counter);
  //   timer_wait_ticks(4);
  // }

  // Smoothly move towards the battery level. Loading the battery will drop the
  // voltage, which will show less LEDs, which will reduce the load, which will
  // increase the voltage, which will show more LEDs, ....
  float smooth_level = 0.0f;
  for (uint8_t counter = 0; counter < 254; ++counter) {
    const uint8_t battery_level = adc_battery_level();
    smooth_level += (battery_level - smooth_level) * 0.02f;
    leds_progress_bar(smooth_level);
    timer_wait_ticks(1);
  }

  for (uint8_t counter = 12; counter > 0; --counter) {
    leds_set_pwm(counter * 4);
    timer_wait_ticks(2);
  }
}

static void fade_to_white()
{
  leds_set_pwm(0);
  leds_write_16bit(~0);
  for (uint8_t counter = 0; counter < 49; ++counter) {
    leds_set_pwm(counter);
    timer_wait_ticks(1);
  }
}
