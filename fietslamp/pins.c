/* Copyright (C) 2021 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "pins.h"

void pins_setup()
{
  DDRA = 1 << DDA3 | 1 << DDA4 | 1 << DDA5;  // OUT on PA3/4/5
  DDRB = 1 << DDB0 | 1 << DDB2;              // OUT on PB0/2
  PORTA = 0;
  PORTB = 0;

  pin_write_blank(false);  // Keep blanking until data has been sent.
}
