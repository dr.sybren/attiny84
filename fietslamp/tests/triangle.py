#!/usr/bin/env python3

from pprint import pprint

leds_level = [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
]

for y in range(4):
    for x in range(4):
        led_index = x + 4 * y
        leds_level[y][x] = (x + 1) * (y + 1) * 15

pprint(leds_level)
