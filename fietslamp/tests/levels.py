#!/usr/bin/env python3

ADC_LOW = 160
ADC_HIGH = 210
input_range = ADC_HIGH - ADC_LOW + 1
output_range = 12

for input in range(ADC_LOW, ADC_HIGH + 1):
    volt = input / 100 * 2
    value = (input - ADC_LOW) * output_range // input_range + 1
    print(f"volt = {volt:4.2f}  ->  ADC={input}  ->  value={value}")
