#include <stdint.h>
#include <stdio.h>

uint32_t xorshift32_state;

/* The state word must be initialized to non-zero */
static uint32_t xorshift32()
{
  /* Algorithm "xor" from p. 4 of Marsaglia, "Xorshift RNGs" */
  uint32_t x = xorshift32_state;
  x ^= x << 13;
  x ^= x >> 17;
  x ^= x << 5;
  xorshift32_state = x;
  return x;
}

uint16_t random_word()
{
  return xorshift32() & 0xFFFF;
}

int main(void)
{
  xorshift32_state = 0xdeadbeef;

  for (int i = 0; i < 12; i++) {
    printf("%04x\n", random_word());
  }
}
