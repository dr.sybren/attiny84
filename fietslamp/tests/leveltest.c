#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

uint8_t leds_level[16];

int main(void)
{
  // const uint8_t light_map_8bit[] = {
  //     0, 1, 2, 3, 5, 7, 10, 15, 21, 29, 40, 54, 74, 102, 138, 188, 255};
  // for (uint8_t y = 0; y < 4; ++y) {
  //   for (uint8_t x = 0; x < 4; ++x) {
  //     const uint8_t led_index = x + 4 * y;
  //     const uint8_t light_map_index = (x + 1) * (y + 1);
  //     const uint8_t led_value = light_map_8bit[light_map_index];
  //     leds_level[led_index] = led_value;
  //     printf("leds_level[%2d] = light_map[%2d] = %d\n",
  //            led_index,
  //            light_map_index,
  //            leds_level[led_index]);
  //   }
  // }

  leds_level[0] = 0;
  leds_level[1] = 0;
  leds_level[2] = 0;
  leds_level[3] = 0;

  leds_level[4] = 1;
  leds_level[5] = 1;
  leds_level[6] = 1;
  leds_level[7] = 1;

  leds_level[8] = 16;
  leds_level[9] = 16;
  leds_level[10] = 16;
  leds_level[11] = 16;

  leds_level[12] = 255;
  leds_level[13] = 255;
  leds_level[14] = 255;
  leds_level[15] = 255;

  {

    for (int16_t random_value = 0; random_value < 256; ++random_value) {
      uint16_t bit_pattern = 0;
      for (int8_t led_index = 0; led_index < 16; ++led_index) {
        const bool led_on = leds_level[led_index] > random_value;
        bit_pattern |= (led_on << led_index);
      }

      printf("%3d -> bit_pattern = %04x\n", random_value, bit_pattern);
    }
  }
}
