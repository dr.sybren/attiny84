/* Copyright (C) 2021 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <stdint.h>

void leds_setup();
void leds_set_pwm(uint8_t light_level);  // values 0 - 48
void leds_write_16bit(uint16_t bit_pattern);
void leds_progress_bar(uint8_t progress);  // values 0 (off) to 12 (full)

// #define LEDS_INDIVIDUAL_PWM
#ifdef LEDS_INDIVIDUAL_PWM
extern uint8_t leds_level[16];
void leds_levels_update();
#endif
