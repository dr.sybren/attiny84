/* Copyright (C) 2021 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <stdbool.h>

#include <avr/io.h>

/* Pinout:
 * PA0: AREF capacitor to ground
 * PA1: AIN Battery voltage
 * PA3: OUT Latch Data
 * PA4: OUT Serial Clock
 * PA5: OUT Serial Data
 * PB0: OUT Status LED
 * PB2: OUT Blank
 */

void pins_setup();

// Write to PA3
static inline void pin_write_latch(bool pin_state)
{
  if (pin_state) {
    PORTA |= 1 << PORTA3;
  }
  else {
    PORTA &= ~(1 << PORTA3);
  }
}

// Write to PA4
static inline void pin_write_serial_clock(bool pin_state)
{
  if (pin_state) {
    PORTA |= 1 << PORTA4;
  }
  else {
    PORTA &= ~(1 << PORTA4);
  }
}

// Write to PA5
static inline void pin_write_serial_data(bool pin_state)
{
  if (pin_state) {
    PORTA |= 1 << PORTA5;
  }
  else {
    PORTA &= ~(1 << PORTA5);
  }
}

// Write to PB0
static inline void pin_write_status_led(bool pin_state)
{
  if (pin_state) {
    PORTB |= 1 << PORTB0;
  }
  else {
    PORTB &= ~(1 << PORTB0);
  }
}

// Write to PB2
static inline void pin_write_blank(bool pin_state)
{
  if (pin_state) {
    PORTB |= 1 << PORTB2;
  }
  else {
    PORTB &= ~(1 << PORTB2);
  }
}
