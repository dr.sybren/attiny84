/* Copyright (C) 2021 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "timekeeping.h"

#include <stdint.h>

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>
#include <util/atomic.h>

volatile uint8_t timer_ticks_since_reset = 0;

void timer_setup()
{
  // WGM = 0b0100 for CTC mode.
  TCCR1A = 0                //
           | 0b00 << WGM10  // WGM bits 0:1
      ;
  TCCR1B = 0                //
           | 0b01 << WGM12  // WGM bits 2:3
           | 0b100 << CS10  // clock select
      ;
  TCCR1C = 0;
  TIMSK1 = 1 << OCIE1A;
  OCR1A = 50;

  timer_reset();
}

void timer_shutdown()
{
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    TCCR1A = TCCR1B = 0;
  }
}

void timer_reset(void)
{
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    TCNT1 = 0;
    timer_ticks_since_reset = 0;
  }
}

void timer_wait_ticks(uint8_t ticks_to_wait)
{
  timer_reset();
  while (timer_ticks_since_reset < ticks_to_wait) {
    // We'll wake up at the TIM0_COMPA interrupt.
    set_sleep_mode(SLEEP_MODE_IDLE);
    sleep_mode();
  }
}

ISR(TIM1_COMPA_vect)
{
  // Avoid overflows.
  if (timer_ticks_since_reset == 255) {
    return;
  }

  ++timer_ticks_since_reset;
}
