/* Copyright (C) 2021 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "adc.h"
#include "pins.h"

#include <avr/interrupt.h>
#include <avr/sleep.h>

void adc_setup(void)
{
  ADMUX = 0                   //
          | 0b10 << REFS0     // 1.1v as voltage ref
          | 0b000001 << MUX0  // Read from ADC1 / PA1
      ;
  ADCSRA = 0                 //
           | 1 << ADEN       // Enable ADC
           | 0 << ADATE      // Auto-trigger
           | 1 << ADIE       // interrupt required for triggering free-running mode
           | 0b100 << ADPS0  // scale ADC clock so it's 50-200 KHz
      ;
  ADCSRB = 0                 //
           | 1 << ADLAR      // Left-adjust
           | 0b000 << ADTS0  // Auto-trigger source
      ;
  DIDR0 = 0             //
          | 1 << ADC0D  // disable digital input on ADC1 / PA1
      ;

  // Trigger a conversion to start freerunning mode.
  // ADCSRA |= 1 << ADSC;
}

ISR(ADC_vect)
{
}

static uint8_t adc_read(const uint8_t iterations)
{
  pin_write_status_led(true);

  uint16_t adc_sum = 0;
  for (int i = 0; i < iterations; ++i) {
    set_sleep_mode(SLEEP_MODE_ADC);
    ADCSRA |= (1 << ADSC);
    while ((ADCSRA & (1 << ADSC)) == 0) {
      sleep_mode();
    }
    adc_sum += ADCH;
  }

  pin_write_status_led(false);
  return adc_sum / iterations;
}

uint8_t adc_rolling_average()
{
  static float adc_rolling = 0;
  const float adc_instant = adc_read(8);

  if (adc_rolling == 0)
    adc_rolling = adc_instant;
  else
    adc_rolling = 0.98f * adc_rolling + 0.02f * adc_instant;

  return adc_rolling;
}

uint8_t adc_battery_level()
{
  const uint8_t adc = adc_read(32);

  constexpr uint8_t ADC_LOW = 194;   // 3.6v level
  constexpr uint8_t ADC_HIGH = 220;  // 4.2v level

  if (adc <= ADC_LOW)
    return 1;
  if (adc >= ADC_HIGH)
    return 12;

  constexpr uint8_t input_range = ADC_HIGH - ADC_LOW;
  constexpr uint8_t output_range = 12;

  const uint8_t level = (adc - ADC_LOW) * output_range / input_range + 1;
  return level;
}
