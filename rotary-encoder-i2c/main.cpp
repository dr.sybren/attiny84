#define F_CPU 16000000

#include <inttypes.h>

#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>

#include "USI_TWI_device.h"

// Pinout:
// PA0:
// PA1: ROT_LEFT
// PA2: ROT_CLICK
// PA3: ALERT (optional)
// PA4: SCK / SCL
// PA5: MISO
// PA6: MOSI / SDA
// PA7:
// PB0: ROT_RIGHT
// PB1:
// PB2: LED
// PB3: RESET

constexpr bool has_alert_pin = true;

// Default I2C address of the TMP1075 IC that I want to mimic with this.
constexpr uint8_t I2C_ADDR = has_alert_pin ? 0x49 : 0x48;
constexpr uint16_t TEMP_DEFAULT = 0x1900;

struct TMP1075Registers {
  union {
    struct {
      uint16_t temp;        // 0x00
      uint16_t config;      // 0x01
      uint16_t low_limit;   // 0x02
      uint16_t high_limit;  // 0x03
      uint16_t _pad_4;      // 0x04
      uint16_t _pad_5;      // 0x05
      uint16_t _pad_6;      // 0x06
      uint16_t _pad_7;      // 0x07
      uint16_t _pad_8;      // 0x08
      uint16_t _pad_9;      // 0x09
      uint16_t _pad_a;      // 0x0a
      uint16_t _pad_b;      // 0x0b
      uint16_t _pad_c;      // 0x0c
      uint16_t _pad_d;      // 0x0d
      uint16_t _pad_e;      // 0x0e
      uint16_t die_id;      // 0x0f
    } fields;
    uint16_t words[16];
  };
};
static TMP1075Registers registers;

static volatile bool flash_led = false;
static volatile bool alert_high = false;

static inline void led_out(bool pin_state);

static void reset()
{
  WDTCSR = (1 << WDCE) | (1 << WDE);
  WDTCSR = (1 << WDE);
  led_out(true);
  for (;;)
    ;
}

static void write_bytes_16(const uint16_t data)
{
  const uint8_t *as_bytes = reinterpret_cast<const uint8_t *>(&data);
  usiTwiTransmitByte(as_bytes[1]);
  usiTwiTransmitByte(as_bytes[0]);
}

static void onReceive(const uint8_t num_bytes_available)
{
}

static void onRequest()
{
  flash_led = true;

  const uint8_t reg_addr = usiTwiReceiveByte();
  if (reg_addr > 0x0f) {
    // Invalid address, just reset.
    reset();
  }
  if (reg_addr == 0x0f) {
    // Reading the ID happens on start, so let's clear any buffers.
    usiFlushBuffers();
  }

  const uint16_t reg_value = registers.words[reg_addr];
  write_bytes_16(reg_value);
}

static inline void led_out(const bool pin_state)
{
  // if (pin_state) {
  //   PORTA |= 1 << PORTA0;
  // }
  // else {
  //   PORTA &= ~(1 << PORTA0);
  // }

  if (pin_state) {
    PORTB |= 1 << PORTB2;
  }
  else {
    PORTB &= ~(1 << PORTB2);
  }
}

static inline void alert_out(const bool alert_state)
{
  if (alert_state) {
    PORTA |= 1 << PORTA3;
  }
  else {
    PORTA &= ~(1 << PORTA3);
  }
}

ISR(PCINT0_vect)
{
  const uint8_t rot_a = PINA & (1 << PINA1);
  const uint8_t rot_click = PINA & (1 << PINA2);
  const uint8_t rot_b = PINB & (1 << PINB0);

  if (!rot_a) {
    registers.fields.temp += (8 * (rot_b ? 1 : -1)) << 4;
    flash_led = true;
  }
  if (!rot_click) {
    if constexpr (has_alert_pin) {
      alert_high = !alert_high;
    }
    else {
      registers.fields.temp = TEMP_DEFAULT;
    }
    flash_led = true;
  }
}

static void setup()
{
  // Set up watchdog.
  MCUSR = 0x00;                        // Clear WDRF in MCUSR.
  WDTCSR |= (1 << WDCE) | (1 << WDE);  // Ensure we can write.
  // WDTCSR = 0                           //
  //          | 1 << WDE                  // Enable watchdog, reset on trigger.
  //          | 0b1001 << WDP0            // 1024 cycles, 8s typical.
  //     ;
  WDTCSR = 0;  // Disable watchdog.

  // Set clock divisor.
  CLKPR = 1 << CLKPCE;
  CLKPR = 0b0000 << CLKPS0;

  OSCCAL = 255;

  PORTA = 0;         // Default to high-Z.
  DDRA = 1 << DDA0;  // LED pin as output.
  if constexpr (has_alert_pin) {
    DDRA |= 1 << DDA3;  // ALERT pin as output.
  }

  PORTA |= 0b11 << PORTA1;  // Pullup on A1-A2.
  PORTB |= 0b1 << PORTB0;   // Pullup on PB0.

  // Set up pin change interrupt for rotary encoder.
  GIMSK = 1 << PCIE0;
  PCMSK0 = 0b111 << PCMSK1;  // Pin change interrupts on A1-A3.

  // Load TMP1075 default values.
  registers.fields.temp = TEMP_DEFAULT;
  registers.fields.config = 0x00FF;
  registers.fields.low_limit = 0x4B00;
  registers.fields.high_limit = 0x5000;
  registers.fields.die_id = 0x7500;

  usi_onReceiverPtr = onReceive;
  usi_onRequestPtr = onRequest;
  usiTwiSlaveInit(I2C_ADDR);
}

static void loop()
{
  usiTwiCheckStop();

  if (flash_led) {
    flash_led = false;

    led_out(true);
    _delay_ms(100);
    led_out(false);
  }

  alert_out(alert_high);
}

int main(void)
{
  setup();
  sei();
  for (;;)
    loop();
}
