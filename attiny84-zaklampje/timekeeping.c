/* Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "timekeeping.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/atomic.h>

volatile uint16_t seconds_since_reset = 0;

void timer_setup()
{
  // WGM = 0b0100 for CTC mode.
  TCCR1A = 0                  //
           | (0b00 << WGM10)  // two LSB of wave generation mode
      ;
  TCCR1B = 0                  //
           | (0b01 << WGM12)  // two MSB of wave generation mode
           | (0b101 << CS10)  // prescaler clk/1024
      ;
  TCCR1C = 0;
  TIMSK1 = 0                //
           | (1 << OCIE1A)  // Output Compare A interrupt enable
      ;

  OCR1A = 13908;  // At room temperature this should result in interrupts at 1 Hz.
  timer_reset();
}

void timer_shutdown()
{
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    TCCR1A = TCCR1B = TCCR1C = TIMSK1 = OCR1A = 0;
  }
}

void timer_reset(void)
{
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    TCNT1 = 0;
    seconds_since_reset = 0;
  }
}

uint16_t timer_seconds_since_reset(void)
{
  return seconds_since_reset;
}

ISR(TIM1_COMPA_vect)
{
  if (seconds_since_reset == 65535) {
    return;
  }

  ++seconds_since_reset;

  // uint8_t old_porta = PORTA;
  // PORTA = 0b01000001;

  // for (uint16_t timer = 0; timer <= 1000; timer++) {
  //   __asm("nop");
  // }

  // PORTA = old_porta;
}
