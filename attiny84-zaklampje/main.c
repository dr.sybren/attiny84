/* ATtiny84 Zaklampje
 * Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "hardware_config.h"
#include "knight_rider.h"
#include "software_pwm.h"
#include "system_init.h"
#include "system_mode.h"
#include "system_mode_sleep.h"
#include "timekeeping.h"

#include <stdbool.h>
#include <stdint.h>

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>

/* Pinout:
 * PA0 - PA6: LEDs
 * PA7 / ADC7: Potentiometer
 * PB0 - PB2: Pushbuttons
 */

ISR(ADC_vect)
{
}

static inline float readADC()
{
  set_sleep_mode(SLEEP_MODE_IDLE);
  ADCSRA |= 1 << ADSC | 1 << ADIE;
  sleep_mode();

  static float adc_value = 0;
  float adc = ADC;
  adc_value += (adc - adc_value) * 0.1f;

  return adc_value;
}

static inline void triangle(float adc_value)
{
  soft_pwm_set_channel(0, adc_value * 2 - 256);
  soft_pwm_set_channel(1, adc_value - 64);
  soft_pwm_set_channel(2, adc_value - 16);
  soft_pwm_set_channel(3, adc_value + 1);
  soft_pwm_set_channel(4, adc_value - 16);
  soft_pwm_set_channel(5, adc_value - 64);
  soft_pwm_set_channel(6, adc_value * 2 - 256);
}

static int8_t swipe_led_num = NUMBER_OF_LEDS / 2;

static inline void show_swipe_led(float adc_value)
{
  for (uint8_t idx = 0; idx < NUMBER_OF_LEDS; idx++) {
    soft_pwm_set_channel(idx, swipe_led_num == idx ? adc_value + 1 : 0);
  }
}

static inline void turn_left(float adc_value)
{
  show_swipe_led(adc_value);
  swipe_led_num = (swipe_led_num - 1) & 0x0F;
}

static inline void turn_right(float adc_value)
{
  show_swipe_led(adc_value);
  swipe_led_num = (swipe_led_num + 1) & 0x0F;
}

#ifdef HAS_PUSHBUTTONS
ISR(PCINT1_vect)
{
  if ((PINB & (1 << PINB0)) == 0) {
    if (system_mode == SYSMODE_TURN_LEFT) {
      system_mode_switch_queue(SYSMODE_TURN_RIGHT);
    }
    else {
      system_mode_switch_queue(SYSMODE_TURN_LEFT);
    }
  }
  else if ((PINB & (1 << PINB1)) == 0) {
    if (system_mode == SYSMODE_TRIANGLE) {
      system_mode_switch_queue(SYSMODE_SLEEPING);
    }
    else {
      system_mode_switch_queue(SYSMODE_TRIANGLE);
    }
  }
  else if ((PINB & (1 << PINB2)) == 0) {
    if (system_mode == SYSMODE_KNIGHT_RIDER) {
      knight_rider_toggle();
    }
    else {
      system_mode_switch_queue(SYSMODE_KNIGHT_RIDER);
    }
  }

  timer_reset();
}
#endif

static inline void loop()
{
  switch (system_mode) {
    case SYSMODE_STARTING_UP:
      system_mode_switch_immediate(SYSMODE_TRIANGLE);
      return;
    case SYSMODE_TRIANGLE:
      triangle(readADC());
      break;
    case SYSMODE_SLEEPING:
      sleep();
      break;
#ifdef HAS_PUSHBUTTONS
    case SYSMODE_TURN_LEFT:
      turn_left(readADC());
      break;
    case SYSMODE_TURN_RIGHT:
      turn_right(readADC());
      break;
    case SYSMODE_KNIGHT_RIDER:
      knight_rider(readADC());
      break;
#endif
  }

  if (timer_seconds_since_reset() >= SLEEP_AFTER_IDLE_SECONDS) {
    system_mode_switch_immediate(SYSMODE_SLEEPING);
    return;
  }

  system_mode_switch();
}

int main()
{
  system_init();
  system_mode = SYSMODE_STARTING_UP;

  sei();
  for (;;)
    loop();
  return 0;
}
