/* Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hardware_config.h"

#ifdef HAS_PUSHBUTTONS

#  include "knight_rider.h"
#  include "software_pwm.h"
#  include "system_mode.h"

#  include <math.h>
#  include <stdint.h>

static int8_t swipe_led_num = NUMBER_OF_LEDS / 2;
static float swipe_led_float = NUMBER_OF_LEDS / 2;
static float min_led_num = 1;
static float led_direction = +1;

static inline float sign(float value)
{
  return value < 0 ? -1.0f : 1.0f;
}

#  define KNIGHT_RIDER_ONE 0.205f
#  define KNIGHT_RIDER_TWO 0.55f

void knight_rider_start()
{
  soft_pwm_set_channels(0);

  led_direction = KNIGHT_RIDER_ONE;
  swipe_led_float = 0 - led_direction;
}

void knight_rider_toggle()
{
  led_direction = sign(led_direction) *
                  ((KNIGHT_RIDER_ONE + KNIGHT_RIDER_TWO) - fabs(led_direction));
}

void knight_rider(float adc_value)
{
  swipe_led_float += led_direction;
  if (swipe_led_float < min_led_num || swipe_led_float >= NUMBER_OF_LEDS) {
    led_direction *= -1;
    swipe_led_float = fmax(min_led_num, fmin(swipe_led_float, NUMBER_OF_LEDS));
  }

  swipe_led_num = (int8_t)swipe_led_float;
  float alpha = swipe_led_float - swipe_led_num;

  adc_value += 3;
  soft_pwm_set_channel(swipe_led_num - 1, (1 - alpha) * adc_value);
  soft_pwm_set_channel(swipe_led_num + 0, alpha * adc_value);
  soft_pwm_set_channel(swipe_led_num - 3, 0);
  soft_pwm_set_channel(swipe_led_num - 2, 0);
  soft_pwm_set_channel(swipe_led_num + 1, 0);
  soft_pwm_set_channel(swipe_led_num + 2, 0);
}

#endif
