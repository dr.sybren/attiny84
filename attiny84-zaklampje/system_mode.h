/* Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include "hardware_config.h"

typedef enum {
  SYSMODE_STARTING_UP,
  SYSMODE_TRIANGLE,
  SYSMODE_SLEEPING,
#ifdef HAS_PUSHBUTTONS
  SYSMODE_TURN_LEFT,
  SYSMODE_TURN_RIGHT,
  SYSMODE_KNIGHT_RIDER,
#endif
} eSystemMode;

#define NUMBER_OF_LEDS 7

extern eSystemMode system_mode;

void system_mode_switch_immediate(eSystemMode go_to_mode);  // immediately switch
void system_mode_switch_queue(eSystemMode go_to_mode);      // switch later
void system_mode_switch();                                  // switch to queued mode
