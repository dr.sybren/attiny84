/* Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "software_pwm.h"

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/atomic.h>

#define NUM_PWM_CHANS 7
static uint16_t channels[NUM_PWM_CHANS] = {0};
static uint16_t channels_queued[NUM_PWM_CHANS] = {0};

static volatile uint16_t pwm_max = 255;
static volatile uint16_t pwm_counter = 0;

void soft_pwm_setup(void)
{
  TCCR0A = 0                 //
           | 0b00 << COM0A0  // OC0A disconnected
           | 0b00 << COM0B0  // OC0B disconnected
           | 0b10 << WGM00   // mode
      ;
  TCCR0B = 0                //
           | 0b0 << WGM02   // mode
           | 0b001 << CS00  // clkdiv
      ;
  TIMSK0 = 0              //
           | 1 << OCIE0A  // Enable interrupt
      ;
  OCR0A = 8;
}

void soft_pwm_shutdown(void)
{
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    TCCR0A = TCCR0B = TIMSK0 = OCR0A = PORTA = 0;
  }
}

ISR(TIM0_COMPA_vect)
{
  pwm_counter++;

  if (pwm_counter > pwm_max) {
    pwm_counter = 0;
    memcpy(channels, channels_queued, sizeof(channels));

    uint8_t enable_chans = 0;
    for (int8_t idx = NUM_PWM_CHANS - 1; idx >= 0; idx--) {
      enable_chans <<= 1;
      enable_chans |= channels[idx] > 0;
    }

    PORTA = enable_chans;
    return;
  }

  uint8_t mask = 0;
  for (int8_t idx = NUM_PWM_CHANS - 1; idx >= 0; idx--) {
    mask <<= 1;
    mask |= channels[idx] > pwm_counter;
  }
  PORTA &= mask;
}

void soft_pwm_set_max(int16_t pwm_max_value)
{
  pwm_max = pwm_max_value;
}

void soft_pwm_set_channel(int8_t channel, int16_t pwm_counter)
{
  if (channel < 0 || channel >= NUM_PWM_CHANS)
    return;
  if (pwm_counter < 0)
    pwm_counter = 0;
  channels_queued[channel] = pwm_counter;
}

void soft_pwm_set_channels(int16_t pwm_counter)
{
  if (pwm_counter < 0)
    pwm_counter = 0;
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    for (int8_t idx = 0; idx < NUM_PWM_CHANS; idx++) {
      channels_queued[idx] = pwm_counter;
    }
  }
}
