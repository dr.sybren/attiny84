/* Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "system_mode.h"
#include "hardware_config.h"
#include "system_mode_sleep.h"
#ifdef HAS_PUSHBUTTONS
#  include "knight_rider.h"
#endif

eSystemMode system_mode;
volatile eSystemMode system_mode_queued;

void enter_current_mode(void)
{
  switch (system_mode) {
    case SYSMODE_STARTING_UP:
      break;
    case SYSMODE_TRIANGLE:
      break;
    case SYSMODE_SLEEPING:
      sleep_start();
      break;
#ifdef HAS_PUSHBUTTONS
    case SYSMODE_TURN_LEFT:
      break;
    case SYSMODE_TURN_RIGHT:
      break;
    case SYSMODE_KNIGHT_RIDER:
      knight_rider_start();
      break;
#endif
  }
}

void exit_current_mode(void)
{
  switch (system_mode) {
    case SYSMODE_STARTING_UP:
      break;
    case SYSMODE_TRIANGLE:
      break;
    case SYSMODE_SLEEPING:
      sleep_stop();
      break;
#ifdef HAS_PUSHBUTTONS
    case SYSMODE_TURN_LEFT:
      break;
    case SYSMODE_TURN_RIGHT:
      break;
    case SYSMODE_KNIGHT_RIDER:
      break;
#endif
  }
}

void system_mode_switch_immediate(eSystemMode go_to_mode)
{
  exit_current_mode();
  system_mode = system_mode_queued = go_to_mode;
  enter_current_mode();
}

void system_mode_switch_queue(eSystemMode go_to_mode)
{
  system_mode_queued = go_to_mode;
}

void system_mode_switch()
{
  if (system_mode_queued == system_mode) {
    return;
  }

  system_mode_switch_immediate(system_mode_queued);
}
