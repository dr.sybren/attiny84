/* Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "software_pwm.h"
#include "system_init.h"
#include "system_mode.h"
#include "timekeeping.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>

void sleep_start(void)
{
  soft_pwm_shutdown();
  timer_shutdown();

  // Disable everything in sleep mode.
  PRR = 0                //
        | (1 << PRTIM1)  //
        | (1 << PRTIM0)  //
        | (1 << PRUSI)   //
        | (1 << PRADC)   //
      ;

  // Turn off all outputs.
  PORTA = 0;

  // Note that pin change interrupts aren't touched here, to make it possible to wake up by
  // pressing a push button.

  // Turn on WDT for the occasional LED blink.
  WDTCSR |= (1 << WDCE) | (1 << WDE);
  WDTCSR = 0                  //
           | (1 << WDIE)      // Watchdog Timeout Interrupt Enable
           | (1 << WDP3)      // MSB of watchdog timer prescaler
           | (0b001 << WDP0)  // other bits of watchdog timer prescaler
      ;
}

void sleep_stop(void)
{
  system_init();
}

void sleep(void)
{
  // Turn on LEDs.
  PORTA = 0b01000001;

  // Wait around a bit.
  for (uint16_t timer = 0; timer <= 1000; timer++) {
    __asm("nop");
  }

  // Turn off all LEDs again.
  PORTA = 0;

  // Power down until a new watchdog interrupts is received.
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  sleep_mode();
}

ISR(WDT_vect)
{
  // Nothing to be done here, waking up is enough.
}
