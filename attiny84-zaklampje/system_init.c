/* Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "system_init.h"
#include "hardware_config.h"
#include "software_pwm.h"
#include "timekeeping.h"

#include <avr/io.h>

static inline void setup_adc(void)
{
  ADMUX = 0                   //
          | 0b00 << REFS0     // VCC as voltage ref
          | 0b000111 << MUX0  // Read from ADC7 / PA7
      ;
  ADCSRA = 0                 //
           | 1 << ADEN       // Enable ADC
           | 1 << ADIE       // Enable interrupt
           | 0b111 << ADPS0  // scale ADC clock so it's 50-200 KHz
      ;
  ADCSRB = 0             //
           | 0 << ADLAR  // Right-align
      ;
  DIDR0 = 0             //
          | 1 << ADC7D  // disable digital input on ADC7 / PA7
      ;
}

void system_init(void)
{
  OSCCAL = 255;  // Highest possible clock from RC

  PRR = 0             //
        | 1 << PRUSI  // Disable USI
      ;
  // Turn off WDT
  WDTCSR |= (1 << WDCE) | (1 << WDE);
  WDTCSR = 0x00;

  // Configure port A.
  DDRA = 0b01111111;  // Everything is output except PA7
  PORTA = 0;

#ifdef HAS_PUSHBUTTONS
  // Port B is used for input on PB0-2.
  DDRB = 0;
  PORTB = 0b111;
  // Pin Change interrupts on PCINT8-10 = PB0-2
  GIMSK |= 1 << PCIE1;
  PCMSK1 |= 0b111 << PCINT8;

  // Wait for the pull-up resistors to do their job and fill up the caps.
  while ((PINB & 0b111) != 0b111)
    ;
#else
  // Port B is not used.
  DDRB = 0;
  PORTB = 0;
#endif

  setup_adc();
  soft_pwm_setup();

  soft_pwm_set_max(512);
  soft_pwm_set_channels(0);

  timer_setup();
}
