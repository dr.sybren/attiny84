/* IR Experiment for ATtiny84
 * Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <math.h>
#include <stdbool.h>
#include <stdint.h>

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>
#include <util/atomic.h>

static inline void setup_pwm()
{
  // Pins PA6 (OC1A) get PWM.
  // CTC mode counting to OCR1A: WGM=0100
  TCCR1A = 0                 //
           | 0b01 << COM1A0  // Toggle OC1A on Compare Match
           | 0b00 << COM1B0  // OCR1B disabled
           | 0b00 << WGM10   //
      ;
  TCCR1B = 0                //
           | 0b01 << WGM12  //
           | 0b001 << CS10  // clk/1
      ;
  TCCR1C = 0;
  TIMSK1 = 0              //
           | 1 << OCIE1A  // Enable interrupt
      ;

  // 16 MHz / 38 KHz = 421.053, so count to 421 / 2 - 1.
  OCR1A = 209;
}

static inline void setup_adc()
{
  ADMUX = 0                   //
          | 0b00 << REFS0     // VCC as voltage ref
          | 0b000000 << MUX0  // Read from ADC0 == PA0
      ;
  ADCSRA = 0                 //
           | 1 << ADEN       // Enable ADC
           | 1 << ADIE       // Enable interrupt
           | 0b011 << ADPS0  // scale ADC clock
      ;
  ADCSRB = 0             //
           | 1 << ADLAR  // Right-align
      ;
  DIDR0 = 0             //
          | 1 << ADC0D  // disable digital input on ADC0 = PA0
      ;
}

static inline void setup()
{
  PRR = 0              //
        | 1 << PRTIM0  // Disable Timer 0
        | 1 << PRUSI   // Disable USI
      ;
  // Turn off WDT
  WDTCSR |= (1 << WDCE) | (1 << WDE);
  WDTCSR = 0x00;

  // Configure port A.
  DDRA = 0            //
         | 1 << DDA1  // for debugging output
         | 1 << DDA6  // for PWM output
      ;
  PORTA = 0                //
          | (0 << PORTA0)  // analogue input
          | (0 << PORTA1)  // output
          | (1 << PORTA2)  // N/C so pull up
          | (1 << PORTA3)  // N/C so pull up
          | (1 << PORTA4)  // N/C so pull up
          | (1 << PORTA5)  // N/C so pull up
          | (0 << PORTA6)  // output
          | (1 << PORTA7)  // N/C so pull up
      ;

  // Port B is not used. Set as input + pullup to save power.
  DDRB = 0;
  PORTB = 0b1111;

  setup_pwm();
  setup_adc();
}

ISR(ADC_vect)
{
}

static inline void readADC()
{
  set_sleep_mode(SLEEP_MODE_IDLE);
  ADCSRA |= 1 << ADSC | 1 << ADIE;
  sleep_mode();
}

static volatile bool pulse_is_on = true;
static volatile uint8_t desired_pulse_on_count = 42;
static volatile uint8_t desired_pulse_off_count = 42;

ISR(TIM1_COMPA_vect)
{
  static uint8_t pulse_count = 0;
  static uint8_t desired_pulse_count = 5;

  pulse_count++;
  if (pulse_count < desired_pulse_count) {
    return;
  }

  if (pulse_is_on) {
    if (PINA & (1 << PINA6)) {
      /* Ignore those compare interrupts where the output is high.
       * We don't want to switch in those cases. */
      return;
    }
    TCCR1A &= ~(0b01 << COM1A0);  // Disable OC1A output
    PORTA &= ~(1 << PORTA1);
    pulse_is_on = false;
    desired_pulse_count = desired_pulse_off_count;
  }
  else {
    TCCR1A |= (0b01 << COM1A0);  // Toggle OC1A on Compare Match
    PORTA |= (1 << PORTA1);
    pulse_is_on = true;
    desired_pulse_count = desired_pulse_on_count - 1;
  }

  TCNT1 = 0;
  pulse_count = 0;
}

static inline void loop()
{
  readADC();
  // OCR1A = ADC;
  // desired_pulse_off_count = (ADCH >> 2);
}

int main()
{
  sei();
  setup();
  for (;;)
    loop();
  return 0;
}
